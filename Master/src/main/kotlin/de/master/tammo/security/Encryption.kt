package de.master.tammo.security

import java.security.MessageDigest
import java.security.SecureRandom
import kotlin.experimental.and

class Encryption {

    companion object {

        /**
         * hash password with salt
         */
        fun hash(password: String, salt: ByteArray): String {
            val md: MessageDigest = MessageDigest.getInstance("MD5")
            md.update(salt)

            val bytes: ByteArray = md.digest(password.toByteArray())

            val builder: StringBuilder = StringBuilder()

            for (byte in bytes) {
                builder.append(Integer.toString((byte and 0xFF.toByte()) + 0x100, 16).substring(1))
            }

            return builder.toString()
        }

        /**
         * check password hash
         */
        fun verify(password: String, hash: String, salt: ByteArray): Boolean {
            return Companion.hash(password, salt).equals(hash)
        }

        /**
         * generate an random salt for new users
         */
        fun generateSalt(): ByteArray {
            val random: SecureRandom = SecureRandom.getInstance("SHA1PRNG", "SUN")
            val salt: ByteArray = ByteArray(16)
            random.nextBytes(salt)
            return salt
        }

    }

}