package de.master.tammo.security.login

class User(val name: String, val password: String, val salt: ByteArray)