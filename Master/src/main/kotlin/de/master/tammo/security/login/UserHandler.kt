package de.master.tammo.security.login

import de.master.tammo.logging.LogLevel
import de.master.tammo.logging.Logger
import de.master.tammo.security.Encryption

class UserHandler {

    /**
     * list of user
     */
    var users: ArrayList<User> = ArrayList()

    /**
     * add user to list
     */
    fun addUser(name: String, password: String) {
        val salt: ByteArray = Encryption.generateSalt()
        this.users.add(User(name, Encryption.hash(password, salt), salt))
    }

    /**
     * setup the first user
     */
    fun createFirstUser() {
        Logger.log("Creating the first user!")
        Logger.log("Type out the name:")
        var name: String = readLine()!!
        Logger.logInput(name)

        while (name.length < 4) {
            Logger.log("Name is to short!", LogLevel.WARNING)
            Logger.log("Type out the name:")
            name = readLine()!!
            Logger.logInput(name)
        }

        Logger.log("Type out the password:")
        var password: String = readLine()!!
        Logger.logInput(password)

        while (password.length < 6) {
            Logger.log("Password is to short!", LogLevel.WARNING)
            Logger.log("Type out the password:")
            password = readLine()!!
            Logger.logInput(password)
        }

        this.addUser(name, password)
        Logger.log("Created user successfully!")
        Logger.log("You are now logged out!")
    }

    /**
     * login into user
     */
    fun login() {
        Logger.log("Logging in:")
        var name: String = readLine()!!
        Logger.logInput(name)

        while (this.users.firstOrNull({ it.name.toLowerCase() == name.toLowerCase() }) == null) {
            Logger.log("Can't find this user!", LogLevel.WARNING)
            Logger.log("Logging in:")
            name = readLine()!!
            Logger.logInput(name)
        }

        val user: User = this.users.first({ it.name.toLowerCase() == name.toLowerCase() })

        Logger.log("Password:")
        var password = readLine()!!
        Logger.logInput(password)

        while (!Encryption.verify(password, user.password, user.salt)) {
            Logger.log("Password is not correct!", LogLevel.WARNING)
            Logger.log("Password:")
            password = readLine()!!
            Logger.logInput(password)
        }

        Logger.log("You are now logged in!")
    }

    /**
     * remove user from list
     */
    fun removeUser(name: String) {
        this.users.remove(this.users.firstOrNull({ it.name.toLowerCase() == name.toLowerCase() }))
    }

}