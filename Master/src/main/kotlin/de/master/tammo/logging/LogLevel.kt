package de.master.tammo.logging

enum class LogLevel {

    DEBUG, INFO, WARNING, ERROR, CRITICAL

}