package de.master.tammo.wrapper

import io.netty.channel.Channel

class WrapperHandler {

    var wrappers: ArrayList<Wrapper>? = ArrayList()

    fun addWrapper(wrapper: Wrapper) {
        this.wrappers?.add(wrapper)
    }

    fun removeWrapper(wrapper: Wrapper) {
        if (this.wrappers?.contains(wrapper)!!) {
            this.wrappers?.remove(wrapper)
        }
    }

    fun nextId(): Int {
        return (1..100).firstOrNull { !this.wrappers?.contains(this.getWrapperById(it))!! } ?: 0
    }

    fun getWrapperByChannel(channel: Channel): Wrapper? {
        return this.wrappers?.firstOrNull({ it.channel == channel })
    }

    fun getWrapperByHost(host: String): Wrapper? {
        return this.wrappers?.firstOrNull({ it.host == host })
    }

    fun getWrapperById(id: Int): Wrapper? {
        return this.wrappers?.firstOrNull({ it.id == id })
    }

}