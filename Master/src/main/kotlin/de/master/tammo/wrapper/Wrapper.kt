package de.master.tammo.wrapper

import io.netty.channel.Channel


class Wrapper(val id: Int, var host: String) {

    init {
        this.host = this.host.replace("localhost", "127.0.0.1")
    }

    @Transient
    var channel: Channel? = null

}