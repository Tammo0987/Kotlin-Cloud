package de.master.tammo.network.packet

import io.netty.buffer.ByteBuf
import io.netty.buffer.ByteBufOutputStream
import io.netty.channel.ChannelHandlerContext
import io.netty.handler.codec.MessageToByteEncoder

class PacketEncoder : MessageToByteEncoder<Packet>() {

    override fun encode(ctx: ChannelHandlerContext?, packet: Packet?, out: ByteBuf?) {
        out!!.writeInt(packet?.id!!)
        packet.write(ByteBufOutputStream(out))
    }

}