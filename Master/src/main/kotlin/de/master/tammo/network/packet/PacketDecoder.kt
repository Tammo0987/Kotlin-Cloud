package de.master.tammo.network.packet

import io.netty.buffer.ByteBuf
import io.netty.buffer.ByteBufInputStream
import io.netty.channel.ChannelHandlerContext
import io.netty.handler.codec.ByteToMessageDecoder

class PacketDecoder : ByteToMessageDecoder() {

    override fun decode(ctx: ChannelHandlerContext?, `in`: ByteBuf?, out: MutableList<Any>?) {
        val packet: Packet = PacketRegistry.getInPacket(`in`?.readInt()!!)
        packet.read(ByteBufInputStream(`in`))
        out?.add(packet)
    }

}