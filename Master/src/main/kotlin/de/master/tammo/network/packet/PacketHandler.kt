package de.master.tammo.network.packet

import de.master.tammo.Master
import de.master.tammo.wrapper.Wrapper
import io.netty.channel.ChannelHandler
import io.netty.channel.ChannelHandlerContext
import io.netty.channel.SimpleChannelInboundHandler

@ChannelHandler.Sharable
class PacketHandler : SimpleChannelInboundHandler<Packet>() {

    private val init: HashMap<Wrapper, ArrayList<Packet>> = HashMap()

    val connectedwrappers: ArrayList<Wrapper> = ArrayList()

    override fun channelActive(ctx: ChannelHandlerContext?) {
        val wrapper: Wrapper = Master.master.wrapperhandler.getWrapperByChannel(ctx?.channel()!!)!!
        if (this.connectedwrappers.contains(wrapper) && this.init.isNotEmpty()) {
            this.init[wrapper]!!.forEach {
                this.sendPacket(it, wrapper)
            }
            this.init.clear()
        }
    }

    override fun channelRead0(ctx: ChannelHandlerContext?, packet: Packet?) {
        val response: Packet? = packet?.handle(ctx?.channel()!!)
        if (response != null) {
            ctx?.channel()!!.writeAndFlush(response)
        }
    }

    override fun exceptionCaught(ctx: ChannelHandlerContext?, cause: Throwable?) {
        cause!!.printStackTrace()
        this.connectedwrappers.remove(Master.master.wrapperhandler.getWrapperByChannel(ctx?.channel()!!))
    }

    fun sendPacket(packet: Packet, wrapper: Wrapper) {
        if (wrapper.channel == null) {
            val packets: ArrayList<Packet> = this.init[wrapper]!!
            packets.add(packet)
            this.init.put(wrapper, packets)
            return
        }

        wrapper.channel!!.writeAndFlush(packet)
    }

}