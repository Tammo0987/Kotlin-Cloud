package de.master.tammo.network.packets.`in`

import de.master.tammo.Master
import de.master.tammo.logging.Logger
import de.master.tammo.network.packet.Packet
import de.master.tammo.wrapper.Wrapper
import io.netty.buffer.ByteBufInputStream
import io.netty.buffer.ByteBufOutputStream
import io.netty.channel.Channel

class WrapperDisconnectInPacket : Packet(5) {

    override fun handle(channel: Channel): Packet? {
        val wrapper: Wrapper = Master.master.wrapperhandler.getWrapperByChannel(channel)!!
        if (Master.master.server.isWrapperConnected(wrapper)) {
            Logger.log("Wrapper-${wrapper.id} disconnected!")
            Master.master.server.disconnectWrapper(wrapper)
        }
        return null
    }

    override fun read(byteBuf: ByteBufInputStream) {}

    override fun write(byteBuf: ByteBufOutputStream) {}
}