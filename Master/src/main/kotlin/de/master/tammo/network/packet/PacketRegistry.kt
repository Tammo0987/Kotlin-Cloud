package de.master.tammo.network.packet

class PacketRegistry {

    companion object {

        private val inpackets: ArrayList<Packet> = ArrayList()

        private val outpackets: ArrayList<Packet> = ArrayList()

        fun registerInPacket(packet: Packet) {
            this.inpackets.add(packet)
        }

        fun registerOutPacket(packet: Packet) {
            this.outpackets.add(packet)
        }

        fun getInPacket(id: Int): Packet {
            return this.inpackets.firstOrNull({ it.id == id })!!
        }
    }

}