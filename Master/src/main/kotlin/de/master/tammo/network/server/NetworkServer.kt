package de.master.tammo.network.server

import de.master.tammo.Master
import de.master.tammo.logging.Logger
import de.master.tammo.network.packet.Packet
import de.master.tammo.network.packet.PacketDecoder
import de.master.tammo.network.packet.PacketEncoder
import de.master.tammo.network.packet.PacketHandler
import de.master.tammo.wrapper.Wrapper
import io.netty.bootstrap.ServerBootstrap
import io.netty.channel.ChannelFuture
import io.netty.channel.ChannelInitializer
import io.netty.channel.ChannelOption
import io.netty.channel.EventLoopGroup
import io.netty.channel.epoll.Epoll
import io.netty.channel.epoll.EpollEventLoopGroup
import io.netty.channel.epoll.EpollServerSocketChannel
import io.netty.channel.nio.NioEventLoopGroup
import io.netty.channel.socket.SocketChannel
import io.netty.channel.socket.nio.NioServerSocketChannel

class NetworkServer(private val port: Int) {

    private val epoll: Boolean = Epoll.isAvailable()

    private lateinit var bossGroup: EventLoopGroup

    private lateinit var workerGroup: EventLoopGroup

    private var packetHandler: PacketHandler = PacketHandler()

    fun bind(): NetworkServer {
        Thread(Runnable {
            this.bossGroup = if (this.epoll) EpollEventLoopGroup() else NioEventLoopGroup()
            this.workerGroup = if (this.epoll) EpollEventLoopGroup() else NioEventLoopGroup()

            try {
                val serverbootstrap = ServerBootstrap()
                        .group(this.bossGroup, this.workerGroup)
                        .channel(if (this.epoll) EpollServerSocketChannel::class.java else NioServerSocketChannel::class.java)
                        .option(ChannelOption.SO_BACKLOG, 128)
                        .childHandler(object : ChannelInitializer<SocketChannel?>() {

                            override fun initChannel(ch: SocketChannel?) {
                                ch?.pipeline()!!.addLast(PacketEncoder()).addLast(PacketDecoder()).addLast(packetHandler)
                                val wrapper: Wrapper = Master.master.wrapperhandler.getWrapperByHost(ch.remoteAddress().address.hostAddress)!!
                                wrapper.channel = ch
                                packetHandler.connectedwrappers.add(wrapper)
                                Logger.log("Wrapper-${wrapper.id} is connected!")
                            }

                        })
                        .childOption(ChannelOption.SO_KEEPALIVE, true)

                val future: ChannelFuture = serverbootstrap.bind(this.port).sync()
                future.channel().closeFuture().syncUninterruptibly()
            } catch (e: Exception) {
                Logger.logException(e, "Can't bind server on port $port")
            }

        }).start()
        return this
    }

    fun sendPacket(packet: Packet, wrapper: Wrapper) {
        this.packetHandler.sendPacket(packet, wrapper)
    }

    fun isWrapperConnected(wrapper: Wrapper): Boolean {
        return this.packetHandler.connectedwrappers.contains(wrapper)
    }

    fun disconnectWrapper(wrapper: Wrapper) {
        this.packetHandler.connectedwrappers.remove(wrapper)
    }

    fun getConnectedWrapper(): Int {
        return this.packetHandler.connectedwrappers.size
    }

    fun close() {
        this.packetHandler.connectedwrappers.clear()
        this.bossGroup.shutdownGracefully()
        this.workerGroup.shutdownGracefully()
        Logger.log("Server was closed!")
    }

}