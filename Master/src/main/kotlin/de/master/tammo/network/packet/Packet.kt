package de.master.tammo.network.packet

import io.netty.buffer.ByteBufInputStream
import io.netty.buffer.ByteBufOutputStream
import io.netty.channel.Channel

abstract class Packet(open val id: Int) {

    abstract fun handle(channel: Channel): Packet?

    abstract fun read(byteBuf: ByteBufInputStream)

    abstract fun write(byteBuf: ByteBufOutputStream)

}