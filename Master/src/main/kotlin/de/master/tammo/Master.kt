package de.master.tammo

import de.master.tammo.command.CommandHandler
import de.master.tammo.config.FileHandler
import de.master.tammo.config.Settings
import de.master.tammo.logging.LogLevel
import de.master.tammo.logging.Logger
import de.master.tammo.network.packet.PacketRegistry
import de.master.tammo.network.packets.`in`.WrapperDisconnectInPacket
import de.master.tammo.network.packets.out.MasterStopOutPacket
import de.master.tammo.network.server.NetworkServer
import de.master.tammo.security.login.UserHandler
import de.master.tammo.wrapper.Wrapper
import de.master.tammo.wrapper.WrapperHandler

fun main(args: Array<String>) {
    Master()
}

class Master {

    companion object {
        lateinit var master: Master
    }

    lateinit var commandhandler: CommandHandler

    lateinit var userhandler: UserHandler

    lateinit var filehandler: FileHandler

    lateinit var wrapperhandler: WrapperHandler

    lateinit var server: NetworkServer

    lateinit var settings: Settings

    private var running: Boolean = false

    init {
        this.printStart()

        this.running = true

        this.init()

        this.setupServer()

        this.setup()

        var line: String
        while (this.running) {
            line = readLine()!!
            this.commandhandler.execute(line)
        }
    }

    private fun init() {
        master = this

        this.commandhandler = CommandHandler()
        this.userhandler = UserHandler()
        this.wrapperhandler = WrapperHandler()

        this.settings = Settings()

        this.filehandler = FileHandler()
    }

    private fun printStart() {
        Logger.log("Starting master", LogLevel.INFO, false)
        this.sleep(500)

        print(".")
        this.sleep(500)

        print(".")
        this.sleep(500)

        println(".")
        this.sleep(500)

        Logger.log(" _______     ____   _      _____   _   _   _____  ")
        Logger.log("|___ ___|   |  __| | |    | ___ | | | | | |  __ | ")
        Logger.log("   | |      | |    | |    ||   || | | | | | |  | |")
        Logger.log("   | |      | |__  | |__  ||___|| | |_| | | |__| |")
        Logger.log("   |_|      |____| |____| |_____| |_____| |_____| ")
        this.sleep()

        Logger.log("")
        this.sleep()

        Logger.log("© Copyright by Tammo0987 2017 - 2018 | Version 1.0-SNAPSHOT")
        this.sleep()

        Logger.log("")
        this.sleep()
    }

    private fun setup() {
        if (this.userhandler.users.isEmpty()) {
            Logger.log("Welcome to the setup!")
            this.userhandler.createFirstUser()
        } else {
            //this.userhandler.login()
        }

        if (this.wrapperhandler.wrappers?.isEmpty()!!) {
            Logger.log("Type in the host of wrapper-1:")
            val host: String = readLine()!!
            Logger.logInput(host)
            val wrapper: Wrapper = Wrapper(this.wrapperhandler.nextId(), host)
            this.wrapperhandler.addWrapper(wrapper)
            Logger.log("Added wrapper-1 on host $host")
            Logger.log("Waiting for connection from wrapper-1...")
            while (!this.server.isWrapperConnected(wrapper)) {
                System.out.flush()
            }
        } else {
            if (this.server.getConnectedWrapper() == 0) {
                Logger.log("Waiting for wrapper to connect!")
                while (this.server.getConnectedWrapper() == 0) {
                    System.out.flush()
                }
            }
        }
    }

    private fun setupServer() {
        PacketRegistry.registerOutPacket(MasterStopOutPacket())

        PacketRegistry.registerInPacket(WrapperDisconnectInPacket())

        this.server = NetworkServer(settings.port).bind()
    }

    fun shutdown() {
        Logger.log("Shutdown master", LogLevel.INFO, false)

        this.wrapperhandler.wrappers!!
                .filter { this.server.isWrapperConnected(it) }
                .forEach { this.server.sendPacket(MasterStopOutPacket(), it) }

        this.sleep(500)

        print(".")
        this.sleep(500)

        print(".")
        this.sleep(500)

        println(".")
        this.sleep(500)

        this.filehandler.files.forEach({ it.save() })

        this.server.close()

        System.exit(0)
    }

    private fun sleep(ms: Long = 100) {
        Thread.sleep(ms)
    }

}