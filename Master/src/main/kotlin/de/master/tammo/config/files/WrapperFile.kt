package de.master.tammo.config.files

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import de.master.tammo.Master
import de.master.tammo.config.GSONFile
import de.master.tammo.wrapper.Wrapper
import java.io.BufferedReader
import java.io.BufferedWriter

class WrapperFile : GSONFile("wrapper") {

    override fun load() {
        val reader: BufferedReader = this.path.bufferedReader()
        Master.master.wrapperhandler.wrappers = Gson().fromJson(reader, object : TypeToken<ArrayList<Wrapper>>() {}.type)
        reader.close()
    }

    override fun save() {
        val writer: BufferedWriter = this.path.bufferedWriter()
        writer.write(GsonBuilder().setPrettyPrinting().create().toJson(Master.master.wrapperhandler.wrappers))
        writer.close()
    }
}