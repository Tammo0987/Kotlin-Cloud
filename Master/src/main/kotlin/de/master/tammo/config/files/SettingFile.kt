package de.master.tammo.config.files

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import de.master.tammo.Master
import de.master.tammo.config.GSONFile
import de.master.tammo.config.Settings
import java.io.BufferedReader
import java.io.BufferedWriter

class SettingFile : GSONFile("settings") {

    override fun load() {
        val reader: BufferedReader = this.path.bufferedReader()
        Master.master.settings = Gson().fromJson(reader, Settings::class.java)!!
        reader.close()
    }

    override fun save() {
        val writer: BufferedWriter = this.path.bufferedWriter()
        writer.write(GsonBuilder().setPrettyPrinting().create().toJson(Master.master.settings))
        writer.close()
    }
}