package de.master.tammo.config

import com.google.common.reflect.ClassPath

class FileHandler {

    /**
     * list of files
     */
    val files: ArrayList<GSONFile> = ArrayList()

    init {
        ClassPath.from(this.javaClass.classLoader).getTopLevelClasses("de.master.tammo.config.files").
                forEach({ this.addFile(Class.forName(it.name).newInstance() as GSONFile) })

        this.files.forEach({ it.load() })
    }

    /**
     * add file to list
     */
    private fun addFile(file: GSONFile) {
        this.files.add(file)
    }

}