package de.master.tammo.config.files

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import de.master.tammo.Master
import de.master.tammo.config.GSONFile
import de.master.tammo.security.login.User
import java.io.BufferedReader
import java.io.BufferedWriter

class UserFile : GSONFile("user") {

    override fun load() {
        val reader: BufferedReader = this.path.bufferedReader()
        Master.master.userhandler.users = Gson().fromJson(reader, object : TypeToken<ArrayList<User>>() {}.type)
        reader.close()
    }

    override fun save() {
        val writer: BufferedWriter = this.path.bufferedWriter()
        writer.write(GsonBuilder().setPrettyPrinting().create().toJson(Master.master.userhandler.users))
        writer.close()
    }
}