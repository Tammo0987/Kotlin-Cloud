package de.master.tammo.command.commands

import de.master.tammo.Master
import de.master.tammo.command.Command
import de.master.tammo.logging.LogLevel
import de.master.tammo.logging.Logger

@Command.CommandInfo("user", ["u"])
class UserCommand : Command {

    override fun execute(args: Array<String>) {
        if (args.isEmpty()) {
            this.printSyntax()
            return
        }

        when (args[0].toLowerCase()) {
            "list" -> Master.master.userhandler.users.forEach({ Logger.log(it.name) })

            "add" -> if (args.size == 3) {
                val name: String = args[1]
                if (name.length > 4) {
                    Logger.log("Name is to short!", LogLevel.WARNING)
                    return
                }

                val password: String = args[2]
                if (password.length < 6) {
                    Logger.log("Password is to short!", LogLevel.WARNING)
                    return
                }

                if (Master.master.userhandler.users.firstOrNull({ it.name.toLowerCase() == name.toLowerCase() }) != null) {
                    Logger.log("This user exists already!", LogLevel.WARNING)
                    return
                }

                Master.master.userhandler.addUser(name, password)
                Logger.log("Created user $name successfully!")

            } else {
                this.printSyntax()
            }

            "remove" -> if (args.size == 2) {
                val name: String = args[1]
                if (Master.master.userhandler.users.firstOrNull({ it.name.toLowerCase() == name.toLowerCase() }) == null) {
                    Logger.log("This user doesn't exists!", LogLevel.WARNING)
                    return
                }

                if (Master.master.userhandler.users.size == 1) {
                    Logger.log("You can't remove the last user!", LogLevel.WARNING)
                    return
                }

                Master.master.userhandler.removeUser(name)
                Logger.log("You removed the user $name successfully!")

            } else {
                this.printSyntax()
            }

            else -> this.printSyntax()
        }
    }

    override fun printSyntax() {
        Logger.log("user list", LogLevel.WARNING)
        Logger.log("user add <name> <password>", LogLevel.WARNING)
        Logger.log("user remove <name>", LogLevel.WARNING)
    }
}