package de.master.tammo.command.commands

import de.master.tammo.Master
import de.master.tammo.command.Command
import de.master.tammo.logging.LogLevel
import de.master.tammo.logging.Logger

@Command.CommandInfo("stop", ["s", "exit", "shutdown"])
class StopCommand : Command {

    override fun execute(args: Array<String>) {
        Master.master.shutdown()
    }

    override fun printSyntax() {
        Logger.log("stop", LogLevel.WARNING)
    }

}