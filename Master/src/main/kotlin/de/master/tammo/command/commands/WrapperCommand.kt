package de.master.tammo.command.commands

import de.master.tammo.Master
import de.master.tammo.command.Command
import de.master.tammo.logging.LogLevel
import de.master.tammo.logging.Logger
import de.master.tammo.wrapper.Wrapper

@Command.CommandInfo("wrapper", ["w"])
class WrapperCommand : Command {

    override fun execute(args: Array<String>) {
        when {
            args.size == 1 -> {
                if (args[0].equals("list", true)) {
                    Logger.log("<--- Wrapper -->")
                    Logger.log("")
                    for (wrapper in Master.master.wrapperhandler.wrappers!!) {
                        Logger.log("Wrapper-${wrapper.id} on Host ${wrapper.host} is" + (if (Master.master.server.isWrapperConnected(wrapper)) "" else " not") + " connected!")
                    }
                } else {
                    this.printSyntax()
                }
            }
            args.size == 2 -> {
                when {
                    args[0].equals("add", true) -> {
                        val host: String = args[1]
                        if (Master.master.wrapperhandler.getWrapperByHost(host) != null) {
                            Logger.log("This wrapper already exists!", LogLevel.WARNING)
                            return
                        }

                        val wrapper: Wrapper = Wrapper(Master.master.wrapperhandler.nextId(), host)
                        Master.master.wrapperhandler.addWrapper(wrapper)
                        Logger.log("Added wrapper-${wrapper.id} on host: $host!")
                    }
                    args[0].equals("remove", true) -> {
                        try {
                            val id: Int = args[1].toInt()
                            if (Master.master.wrapperhandler.getWrapperById(id) != null) {
                                val wrapper: Wrapper = Master.master.wrapperhandler.getWrapperById(id)!!
                                if (Master.master.server.isWrapperConnected(wrapper)) {
                                    Logger.log("Please shutdown wrapper before removing!", LogLevel.WARNING)
                                    return
                                }
                                Master.master.wrapperhandler.removeWrapper(wrapper)
                                Logger.log("Wrapper-${wrapper.id} was removed!")
                            } else {
                                Logger.log("This wrapper doesn't exists!")
                            }
                        } catch (e: NumberFormatException) {
                            Logger.log("That is not a valid number!", LogLevel.WARNING)
                        }
                    }
                    else -> this.printSyntax()
                }
            }
            else -> this.printSyntax()
        }
    }

    override fun printSyntax() {
        Logger.log("wrapper list", LogLevel.WARNING)
        Logger.log("wrapper add <host>", LogLevel.WARNING)
        Logger.log("wrapper remove <id>", LogLevel.WARNING)
    }
}