package de.master.tammo.command

import com.google.common.reflect.ClassPath
import de.master.tammo.logging.Logger
import de.master.tammo.utils.Utils

class CommandHandler {

    /**
     * list of all commands
     */
    val commands: ArrayList<Command> = ArrayList()

    /**
     * put all commands from package into list
     */
    init {
        ClassPath.from(this.javaClass.classLoader).getTopLevelClasses("de.master.tammo.command.commands")
                .filter { Command::class.java.isAssignableFrom(Class.forName(it.name)) }
                .forEach { this.addCommand(Class.forName(it.name).newInstance() as Command) }
    }

    /**
     * executes the command by name
     */
    fun execute(input: String) {
        if (!Utils.isStringValid(input)) {
            return
        }

        Logger.logInput(input)

        for (command in this.commands) {
            if (command.javaClass.isAnnotationPresent(Command.CommandInfo::class.java)) {
                val info: Command.CommandInfo = command.javaClass.getAnnotation(Command.CommandInfo::class.java)
                val aliases: List<String> = info.aliases.toList().plusElement(info.name)
                var arguments: List<String> = input.split(" ")

                for (alias in aliases) {
                    if (arguments[0].equals(alias, true)) {
                        arguments = arguments.minusElement(arguments[0])
                        command.execute(arguments.toTypedArray())
                        return
                    }
                }
            }
        }
        Logger.log("Command not found!")
        Logger.log("Try help")
    }

    /**
     * adding a command
     */
    private fun addCommand(command: Command) {
        this.commands.add(command)
    }

}