package de.master.tammo.command

interface Command {

    /**
     * execute command
     */
    fun execute(args: Array<String>)

    /**
     * print syntax to console
     */
    fun printSyntax()

    /**
     * annotation for command info
     */
    @Retention(AnnotationRetention.RUNTIME)
    @Target(AnnotationTarget.CLASS)
    annotation class CommandInfo(val name: String, val aliases: Array<String>)

}