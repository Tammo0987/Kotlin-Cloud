package de.master.tammo.command.commands

import de.master.tammo.Master
import de.master.tammo.command.Command
import de.master.tammo.logging.Logger

@Command.CommandInfo("help", ["h"])
class HelpCommand : Command {

    override fun execute(args: Array<String>) {
        Logger.log("<--Help-->")
        Logger.log("")
        for (command in Master.master.commandhandler.commands) {
            if (command.javaClass.isAnnotationPresent(Command.CommandInfo::class.java)) {
                Logger.log(command.javaClass.getAnnotation(Command.CommandInfo::class.java).name)
            }
        }
    }

    override fun printSyntax() {}
}