package de.wrapper.tammo

import de.wrapper.tammo.command.CommandHandler
import de.wrapper.tammo.config.FileHandler
import de.wrapper.tammo.config.Settings
import de.wrapper.tammo.logging.LogLevel
import de.wrapper.tammo.logging.Logger
import de.wrapper.tammo.network.client.NetworkClient
import de.wrapper.tammo.network.packet.PacketRegistry
import de.wrapper.tammo.network.packets.`in`.MasterStopInPacket
import de.wrapper.tammo.network.packets.out.WrapperDisconnectOutPacket
import de.wrapper.tammo.proxy.ProxyHandler
import de.wrapper.tammo.workload.Workload

fun main(args: Array<String>) {
    Wrapper()
}

class Wrapper {

    companion object {
        lateinit var wrapper: Wrapper
    }

    lateinit var commandhandler: CommandHandler

    lateinit var filehandler: FileHandler

    lateinit var networkclient: NetworkClient

    lateinit var proxyhandler: ProxyHandler

    lateinit var settings: Settings

    lateinit var workload: Workload

    var running: Boolean = false

    init {
        this.printStart()

        this.running = true

        this.init()

        this.setupNetwork()

        this.proxyhandler.start()

        this.workload.start()

        var line: String
        while (this.running) {
            line = readLine()!!
            this.commandhandler.execute(line)
        }
    }

    private fun init() {
        wrapper = this

        this.commandhandler = CommandHandler()

        this.settings = Settings()

        this.filehandler = FileHandler()

        this.proxyhandler = ProxyHandler()

        this.workload = Workload()
    }

    private fun setupNetwork() {
        PacketRegistry.registerInPacket(MasterStopInPacket())

        PacketRegistry.registerOutPacket(WrapperDisconnectOutPacket())

        this.networkclient = NetworkClient(this.settings.host, this.settings.port).connect()
    }

    private fun printStart() {
        Logger.log("Starting wrapper", LogLevel.INFO, false)
        this.sleep(500)

        print(".")
        this.sleep(500)

        print(".")
        this.sleep(500)

        println(".")
        this.sleep(500)

        Logger.log(" _______     ____   _      _____   _   _   _____  ")
        Logger.log("|___ ___|   |  __| | |    | ___ | | | | | |  __ | ")
        Logger.log("   | |      | |    | |    ||   || | | | | | |  | |")
        Logger.log("   | |      | |__  | |__  ||___|| | |_| | | |__| |")
        Logger.log("   |_|      |____| |____| |_____| |_____| |_____| ")
        this.sleep()

        Logger.log("")
        this.sleep()

        Logger.log("© Copyright by Tammo0987 2017 - 2018 | Version 1.0-SNAPSHOT")
        this.sleep()

        Logger.log("")
        this.sleep()
    }

    private fun sleep(ms: Long = 100) {
        Thread.sleep(ms)
    }

    fun shutdown() {
        Logger.log("Shutdown wrapper", LogLevel.INFO, false)
        this.networkclient.sendPacket(WrapperDisconnectOutPacket())
        this.sleep(500)

        print(".")
        this.sleep(500)

        print(".")
        this.sleep(500)

        println(".")
        this.sleep(500)

        this.proxyhandler.stop()

        this.filehandler.files.forEach({ it.save() })

        this.networkclient.disconnect()

        System.exit(0)
    }

}