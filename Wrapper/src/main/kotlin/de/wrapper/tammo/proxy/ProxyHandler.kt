package de.wrapper.tammo.proxy

import de.wrapper.tammo.logging.Logger
import de.wrapper.tammo.utils.Utils
import java.io.*
import java.nio.charset.StandardCharsets
import java.nio.file.Files

class ProxyHandler {

    private val proxy: File = File("Wrapper//Proxy//Waterfall.jar")

    private lateinit var process: Process

    lateinit var reader: BufferedReader

    lateinit var writer: BufferedWriter

    private lateinit var thread: Thread

    init {
        if (!this.proxy.parentFile.exists()) {
            Files.createDirectories(this.proxy.parentFile.toPath())
        }
        if (!this.proxy.exists()) {
            Utils.download("https://ci.destroystokyo.com/job/Waterfall/lastSuccessfulBuild/artifact/Waterfall-Proxy/bootstrap/target/Waterfall.jar", "Wrapper//Proxy//Waterfall.jar")
        }
    }

    fun start() {
        this.thread = Thread(Runnable {

            val builder: ProcessBuilder = ProcessBuilder("java", "-jar", "Waterfall.jar").directory(this.proxy.parentFile)

            try {
                this.process = builder.start()

                this.reader = BufferedReader(InputStreamReader(this.process.inputStream, StandardCharsets.UTF_8))
                this.writer = BufferedWriter(OutputStreamWriter(this.process.outputStream, StandardCharsets.UTF_8))

                Logger.log("Proxy started!")
            } catch (e: IOException) {
                Logger.logException(e, "Can't start proxy!")
            }

        }, "proxy thread")
        this.thread.start()
    }

    fun dispatchCommand(command: String) {
        this.writer.write(command)
        this.writer.newLine()
        this.writer.flush()

        Logger.log("Dispatch command $command on proxy!")
    }

    fun stop() {
        this.process.destroy()
        this.reader.close()
        this.writer.close()
        this.thread.interrupt()
        Logger.log("Proxy stopped successfully!")
    }

}