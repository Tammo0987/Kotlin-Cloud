package de.wrapper.tammo.command.commands

import de.wrapper.tammo.Wrapper
import de.wrapper.tammo.command.Command
import de.wrapper.tammo.logging.LogLevel
import de.wrapper.tammo.logging.Logger

@Command.CommandInfo("stop", ["s", "exit", "shutdown"])
class StopCommand : Command {

    override fun execute(args: Array<String>) {
        Wrapper.wrapper.shutdown()
    }

    override fun printSyntax() {
        Logger.log("stop", LogLevel.WARNING)
    }
}