package de.wrapper.tammo.server

import de.wrapper.tammo.logging.Logger
import de.wrapper.tammo.utils.Utils
import java.io.*
import java.nio.charset.StandardCharsets

class Server(private val name: String, private val port: Int, private val vmArgs: Array<String>, private val serverArgs: Array<String>) {

    private lateinit var thread: Thread

    private lateinit var process: Process

    private lateinit var reader: BufferedReader

    private lateinit var writer: BufferedWriter

    fun start() {
        this.thread = Thread(Runnable {

            val global = File("Wrapper//Global")
            val tempDir: File = File("Wrapper//temp//${this.name}")

            Utils.copyDir(global, tempDir)
            //TODO copy Template

            val builder: ProcessBuilder = ProcessBuilder(this.parseStartupString()).directory(tempDir)

            try {
                this.process = builder.start()

                this.reader = BufferedReader(InputStreamReader(this.process.inputStream, StandardCharsets.UTF_8))
                this.writer = BufferedWriter(OutputStreamWriter(this.process.outputStream, StandardCharsets.UTF_8))
            } catch (e: IOException) {
                Logger.logException(e, "Error while starting server ${this.name}!")
            }


        }, "gameserver ${this.name}")
        this.thread.start()
    }

    fun dispatch(command: String) {
        this.writer.write(command)
        this.writer.newLine()
        this.writer.flush()
    }

    fun stop() {
        this.process.destroy()
        this.reader.close()
        this.writer.close()
        this.thread.interrupt()
        Logger.log("Stopped server ${this.name}!")
    }

    private fun parseStartupString(): ArrayList<String> {
        val list = ArrayList<String>()
        list.add("java")
        list.addAll(this.vmArgs)
        list.add("-jar")
        list.add("spigot.jar")
        list.add("-port")
        list.add(this.port.toString())
        list.addAll(this.serverArgs)
        return list
    }

}