package de.wrapper.tammo.logging

enum class LogLevel {

    DEBUG, INFO, WARNING, ERROR, CRITICAL

}