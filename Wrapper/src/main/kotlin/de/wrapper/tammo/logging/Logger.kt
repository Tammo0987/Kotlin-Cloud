package de.wrapper.tammo.logging

import java.io.BufferedWriter
import java.io.File
import java.io.FileWriter
import java.io.PrintWriter
import java.nio.file.Files
import java.text.SimpleDateFormat
import java.util.*

class Logger {

    companion object {

        /**
         * prefix
         */
        private val PREFIX: String = "\u2B9E T-Cloud \u2B62"

        /**
         * timestemp for info
         */
        private val TIMESTEMP: SimpleDateFormat = SimpleDateFormat("HH:mm:ss")

        /**
         * fileformat to create logfiles
         */
        private val FILE_FORMAT: SimpleDateFormat = SimpleDateFormat("yyyy//MM//dd")

        /**
         * logs content to console
         */
        fun log(content: Any, level: LogLevel = LogLevel.INFO, line: Boolean = true) {
            val time: String = TIMESTEMP.format(Date())
            var logcontent = "$time $PREFIX "
            logcontent += when (level) {
                LogLevel.DEBUG -> "[DEBUG] $content"
                LogLevel.INFO -> "$content"
                LogLevel.WARNING -> "[WARNING] $content"
                LogLevel.ERROR -> "[ERROR] $content"
                LogLevel.CRITICAL -> "[CRITICAL] $content"
            }

            val error = level == LogLevel.ERROR || level == LogLevel.CRITICAL

            if (line) println(logcontent) else print(logcontent)
            logToFile(logcontent, error)
        }

        /**
         * log exception
         */
        fun logException(exception: Exception, error: Any) {
            log(error, LogLevel.ERROR)
            log(exception.localizedMessage, LogLevel.ERROR)
        }

        /**
         * log input from console to file
         */
        fun logInput(input: Any) {
            val time: String = TIMESTEMP.format(Date())
            val content = "$time $PREFIX [INPUT] $input"
            logToFile(content)
        }

        /**
         * log object to file
         */
        fun logToFile(content: Any, error: Boolean = false) {
            val file = File("Log//Wrapper//${FILE_FORMAT.format(Date())}." + if (error) "error" else "log")

            if (!file.parentFile.exists()) {
                Files.createDirectories(file.parentFile.toPath())
            }

            if (!file.exists()) {
                Files.createFile(file.toPath())
            }

            writeToFile(file, content)
        }

        /**
         * write line to file at the end
         */
        private fun writeToFile(file: File, content: Any) {
            val writer = PrintWriter(BufferedWriter(FileWriter(file, true)))
            writer.println(content)
            writer.close()
        }

    }

}