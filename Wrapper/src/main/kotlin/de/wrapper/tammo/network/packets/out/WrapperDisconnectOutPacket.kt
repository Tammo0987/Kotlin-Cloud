package de.wrapper.tammo.network.packets.out

import de.wrapper.tammo.network.packet.Packet
import io.netty.buffer.ByteBufInputStream
import io.netty.buffer.ByteBufOutputStream
import io.netty.channel.Channel

class WrapperDisconnectOutPacket : Packet(5) {

    override fun handle(channel: Channel): Packet? {
        return null
    }

    override fun read(byteBuf: ByteBufInputStream) {}

    override fun write(byteBuf: ByteBufOutputStream) {}
}