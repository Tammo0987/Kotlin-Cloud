package de.wrapper.tammo.network.client

import de.wrapper.tammo.Wrapper
import de.wrapper.tammo.logging.LogLevel
import de.wrapper.tammo.logging.Logger
import de.wrapper.tammo.network.packet.Packet
import de.wrapper.tammo.network.packet.PacketDecoder
import de.wrapper.tammo.network.packet.PacketEncoder
import de.wrapper.tammo.network.packet.PacketHandler
import io.netty.bootstrap.Bootstrap
import io.netty.channel.*
import io.netty.channel.epoll.Epoll
import io.netty.channel.epoll.EpollEventLoopGroup
import io.netty.channel.epoll.EpollSocketChannel
import io.netty.channel.nio.NioEventLoopGroup
import io.netty.channel.socket.nio.NioSocketChannel

class NetworkClient(private val host: String, private val port: Int) {

    private val epoll: Boolean = Epoll.isAvailable()

    private lateinit var workerGroup: EventLoopGroup

    private val packethandler: PacketHandler = PacketHandler()

    var connected = false

    fun connect(): NetworkClient {

        Thread(Runnable {

            this.workerGroup = if (this.epoll) EpollEventLoopGroup() else NioEventLoopGroup()

            try {
                val bootstrap: Bootstrap = Bootstrap()
                        .group(this.workerGroup)
                        .channel(if (this.epoll) EpollSocketChannel::class.java else NioSocketChannel::class.java)
                        .option(ChannelOption.SO_KEEPALIVE, true)
                        .handler(object : ChannelInitializer<Channel>() {

                            override fun initChannel(ch: Channel?) {
                                ch?.pipeline()!!.addLast(PacketDecoder()).addLast(PacketEncoder()).addLast(packethandler)
                            }

                        })

                val future: ChannelFuture = bootstrap.connect(this.host, this.port).sync()

                this.connected = true

                Logger.log("Connected to master!")

                future.channel().closeFuture().syncUninterruptibly()
            } catch (e: Exception) {
                Logger.logException(e, "Can't connect to master on $host on port $port")
                Logger.log("Please start the master before wrapper!", LogLevel.ERROR)
                Wrapper.wrapper.shutdown()
            } finally {
                this.disconnect()
            }

        }).start()

        return this
    }

    fun sendPacket(packet: Packet) {
        this.packethandler.sendPacket(packet)
    }

    fun disconnect() {
        this.workerGroup.shutdownGracefully()
        this.connected = false
    }

}