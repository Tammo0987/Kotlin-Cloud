package de.wrapper.tammo.network.packets.`in`

import de.wrapper.tammo.Wrapper
import de.wrapper.tammo.logging.Logger
import de.wrapper.tammo.network.packet.Packet
import io.netty.buffer.ByteBufInputStream
import io.netty.buffer.ByteBufOutputStream
import io.netty.channel.Channel

class MasterStopInPacket : Packet(5) {

    override fun handle(channel: Channel): Packet? {
        Logger.log("Master went down")
        Wrapper.wrapper.shutdown()
        return null
    }

    override fun read(byteBuf: ByteBufInputStream) {}

    override fun write(byteBuf: ByteBufOutputStream) {}
}