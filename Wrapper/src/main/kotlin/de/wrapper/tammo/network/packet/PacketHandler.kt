package de.wrapper.tammo.network.packet

import de.wrapper.tammo.Wrapper
import de.wrapper.tammo.logging.Logger
import io.netty.channel.Channel
import io.netty.channel.ChannelHandler
import io.netty.channel.ChannelHandlerContext
import io.netty.channel.SimpleChannelInboundHandler

@ChannelHandler.Sharable
class PacketHandler : SimpleChannelInboundHandler<Packet>() {

    private var channel: Channel? = null

    private val init: ArrayList<Packet> = ArrayList()

    override fun channelActive(ctx: ChannelHandlerContext?) {
        this.channel = ctx?.channel()!!

        if (this.channel != null) {
            if (this.init.isNotEmpty()) {
                this.init.forEach { this.sendPacket(it) }
                this.init.clear()
            }
        }
    }

    override fun channelRead0(ctx: ChannelHandlerContext?, packet: Packet?) {
        val response: Packet? = packet?.handle(ctx?.channel()!!)
        if (response != null) {
            this.channel!!.writeAndFlush(response)
        }
    }

    override fun exceptionCaught(ctx: ChannelHandlerContext?, cause: Throwable?) {
        Logger.log("Server went down!")
        Wrapper.wrapper.shutdown()
    }

    fun sendPacket(packet: Packet) {
        if (this.channel == null) {
            this.init.add(packet)
            return
        }

        this.channel?.writeAndFlush(packet)
    }

}