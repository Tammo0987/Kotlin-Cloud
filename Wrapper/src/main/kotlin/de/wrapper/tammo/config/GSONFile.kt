package de.wrapper.tammo.config

import java.io.File
import java.nio.file.Files

abstract class GSONFile(name: String) {

    /**
     * file to save
     */
    protected var path: File = File("Wrapper//Config//$name.json")

    init {
        if (Files.notExists(this.path.toPath().parent)) {
            Files.createDirectories(this.path.toPath().parent)
        }

        if (Files.notExists(this.path.toPath())) {
            Files.createFile(this.path.toPath())
            this.save()
        }
    }

    /**
     * load file
     */
    abstract fun load()

    /**
     * save file
     */
    abstract fun save()

}
