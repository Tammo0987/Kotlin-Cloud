package de.wrapper.tammo.config.files

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import de.wrapper.tammo.Wrapper
import de.wrapper.tammo.config.GSONFile
import de.wrapper.tammo.config.Settings
import java.io.BufferedReader
import java.io.BufferedWriter

class SettingFile : GSONFile("settings") {

    override fun load() {
        val reader: BufferedReader = this.path.bufferedReader()
        Wrapper.wrapper.settings = Gson().fromJson(reader, Settings::class.java)
        reader.close()
    }

    override fun save() {
        val writer: BufferedWriter = this.path.bufferedWriter()
        writer.write(GsonBuilder().setPrettyPrinting().create().toJson(Wrapper.wrapper.settings))
        writer.close()
    }


}