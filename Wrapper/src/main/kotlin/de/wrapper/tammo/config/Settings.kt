package de.wrapper.tammo.config

class Settings {

    val host: String = "127.0.0.1"

    val port: Int = 7777

}