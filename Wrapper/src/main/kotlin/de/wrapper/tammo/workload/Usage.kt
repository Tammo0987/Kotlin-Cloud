package de.wrapper.tammo.workload

data class Usage(val systemCPU: Int, val processCPU: Int, val ram: Long)