package de.wrapper.tammo.workload

import com.sun.management.OperatingSystemMXBean
import de.wrapper.tammo.Wrapper
import java.lang.management.ManagementFactory

class Workload {

    private val os: OperatingSystemMXBean = ManagementFactory.getOperatingSystemMXBean() as OperatingSystemMXBean

    private val usage: ArrayList<Usage> = ArrayList()

    private lateinit var thread: Thread

    fun start() {
        this.thread = Thread(Runnable {
            Runtime.getRuntime().addShutdownHook(Thread(this.thread::interrupt))

            while (Wrapper.wrapper.running) {
                if (this.usage.size > 10) {
                    this.usage.removeAt(0)
                }

                this.usage.add(Usage(this.getSystemCPU(), this.getProcessCPU(), this.getRamInMb()))

                Thread.sleep(3000)
            }

        }, "workload")
        this.thread.start()
    }

    fun getLoad(): Usage {
        var system = 0
        var process = 0
        var ram: Long = 0

        for (usage in this.usage) {
            system += usage.systemCPU
            process += usage.processCPU
            ram += usage.ram
        }

        system /= usage.size
        process /= usage.size
        ram /= usage.size

        return Usage(system, process, ram)
    }

    private fun getSystemCPU(): Int {
        return (this.os.systemCpuLoad * 100).toInt()
    }

    private fun getProcessCPU(): Int {
        return (this.os.processCpuLoad * 100).toInt()
    }

    private fun getRamInMb(): Long {
        return this.os.freePhysicalMemorySize / (1024 * 1024)
    }

}