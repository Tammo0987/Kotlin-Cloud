package de.wrapper.tammo.utils

import de.wrapper.tammo.logging.LogLevel
import de.wrapper.tammo.logging.Logger
import java.io.File
import java.io.FileOutputStream
import java.net.HttpURLConnection
import java.net.URL
import java.nio.channels.Channels
import java.nio.channels.ReadableByteChannel
import java.nio.file.Files
import java.nio.file.StandardCopyOption

class Utils {

    companion object {

        /**
         * check if only contains spaces
         */
        fun isStringValid(string: String): Boolean {
            return string.trim().isNotEmpty()
        }

        /**
         * download file from url
         */
        fun download(url: String, path: String) {
            Logger.log("Downloading ${path.split("//")[path.split("//").size - 1]}...")
            val weburl = URL(url)
            val httpconnection: HttpURLConnection = weburl.openConnection() as HttpURLConnection
            httpconnection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11")

            if (httpconnection.responseCode == HttpURLConnection.HTTP_OK) {
                val rbc: ReadableByteChannel = Channels.newChannel(httpconnection.inputStream)
                val fos = FileOutputStream(path)
                fos.channel.transferFrom(rbc, 0, Long.MAX_VALUE)
                rbc.close()
                fos.close()

                Logger.log("Download successfully!")
            } else {
                Logger.log("Bad download...", LogLevel.ERROR)
            }

            httpconnection.disconnect()
        }

        fun copyDir(from: File, to: File) {
            if (!to.exists()) {
                Files.createDirectories(to.toPath())
            }
            for (file in from.listFiles()) {
                if (file.isDirectory) {
                    copyDir(file, File("${to.absolutePath}//${file.name}"))
                } else {
                    Files.copy(file.toPath(), File("${to.absolutePath}//${file.name}").toPath(), StandardCopyOption.REPLACE_EXISTING)
                }
            }
        }

        fun deleteDir(dir: File) {
            for (file in dir.listFiles()) {
                if (file.isDirectory) {
                    deleteDir(file)
                } else {
                    Files.delete(file.toPath())
                }
            }
        }
    }

}